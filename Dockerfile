FROM ubuntu:20.04
LABEL maintainer="László Hegedűs"

ENV PYTHONIOENCODING UTF-8
ENV DEBIAN_FRONTEND noninteractive

# Install dependencies.
RUN apt update \
    && apt install -y --no-install-recommends \
       apt-utils \
       locales \
       software-properties-common \
       rsyslog systemd systemd-cron sudo iproute2 \
    && rm -Rf /var/lib/apt/lists/* \
    && rm -Rf /usr/share/doc && rm -Rf /usr/share/man \
    && apt-get clean
RUN sed -i 's/^\($ModLoad imklog\)/#\1/' /etc/rsyslog.conf

# Fix potential UTF-8 errors with ansible-test.
RUN locale-gen en_US.UTF-8

#RUN add-apt-repository -y ppa:deadsnakes/ppa
RUN apt-get update


RUN apt-get install -y --no-install-recommends \
    python3 \
    python3-pip

#RUN ln -s /usr/bin/python3.8 /usr/bin/python 
#RUN ln -s /usr/bin/pip3 /usr/bin/pip

#RUN pip3 -U pip setuptools setuptools-rust

#RUN echo "Python: $(python --version)\n" && echo "Pip: $(pip --version)\n"

COPY initctl_faker .
RUN chmod +x initctl_faker && rm -fr /sbin/initctl && ln -s /initctl_faker /sbin/initctl

# Install Ansible inventory file.
RUN mkdir -p /etc/ansible
RUN echo "[local]\nlocalhost ansible_connection=local" > /etc/ansible/hosts

# RUN useradd -rm -d /home/ansible -s /bin/bash -g root -G sudo -u 1001 ansible
# RUN echo 'ansible:qwe123' | chpasswd
# USER ansible
# WORKDIR /home/ansible

# Remove unnecessary getty and udev targets that result in high CPU usage when using
# multiple containers with Molecule (https://github.com/ansible/molecule/issues/1104)
RUN rm -f /lib/systemd/system/systemd*udev* \
  && rm -f /lib/systemd/system/getty.target

# RUN groupadd ansible
# RUN useradd -rm -d /home/ansible -s /bin/bash -g root -G sudo -u 1002 ansible 
# RUN echo "ansible:qwe123" | chpasswd
# RUN adduser ansible sudo
# USER ansible
# WORKDIR /home/ansible

#COPY --chown=root:root requirements.txt requirements.txt
#RUN python -m pip install --user --upgrade pip setuptools setuptools-rust
#RUN python -m pip install --user -r requirements.txt


# ENV PATH="/home/ansible/.local/bin:${PATH}"
# #COPY --chown=ansible:ansible . .







#RUN useradd -m docker && echo "docker:docker" | chpasswd && adduser docker sudo



VOLUME ["/sys/fs/cgroup", "/tmp", "/run"]
CMD ["/lib/systemd/systemd"]

